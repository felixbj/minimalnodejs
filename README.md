## Description

Minimal Node.js, Express, & Babel Setup

## Used npm content

- @babel/node @babel/core @babel/preset-env @babel/cli
- nodemon
- express
- logger-nodejs


npm i @babel/core @babel/cli @babel/cli @babel/node @babel/preset-env
npm i nodemon
npm i express
npm i dotenv
npm i logger-nodejs
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start:dev

# production mode
$ npm run build
$ npm run start:prod

#purge
$ npmm run purge
```
# Autor: 
 Felix Bejarano Perez

# Contacto: 
 felix.bj89@gmail.com