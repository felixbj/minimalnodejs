import apiServices from "../services/apiServices"
import Logger from "logger-nodejs"

const logger = new Logger();

const apiControllers = {
    getAll: function (req, res) {
        logger.info("GET /api");
        const api = apiServices.getAll();
        res.status(api.status).json({ mensaje: api.mensaje });
    },
    getOne: function (req, res) {
        logger.info(`GET /api/:${ req.params.id }`);
        const api = apiServices.getOne(req);
        res.status(api.status).json({ mensaje: api.mensaje });
    },
    saveOne: function (req, res) {
        logger.info("POST /api");
        const api = apiServices.saveOne(req);
        res.status(api.status).json({ message: api.mensaje });
    },
    updateOne: function (req, res) {
        const id = req.params.id;
        if (!id) {
            logger.error(`PUT El parámetro id es requerido`);
            res.status(400).json({ mensaje: 'El parámetro id es requerido' });
        } else {
            logger.info(`PUT /api/:${ req.params.id }`);
            const api = apiServices.updateOne(req);
            res.status(api.status).json({ mensaje: api.mensaje });
        }
    },
    deleteOne: function (req, res) {
        const id = req.params.id;
        if (!id) {
            logger.error(`DELETE El parámetro id es requerido`);
            res.status(400).json({ mensaje: 'El parámetro id es requerido' });
        } else {
            logger.info(`DELETE /api/:${ req.params.id }`);
            const api = apiServices.deleteOne(req);
            res.status(api.status).json({ mensaje: api.mensaje });
        }
    },
};

export default apiControllers;