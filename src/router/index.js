import express from "express"

const indexRouter = express.Router();

indexRouter.get('/', function (req, res) { res.send('Hola Minimmo proyecto Js'); });
indexRouter.get('/about', function (req, res) { res.status(200).send('About Minimmo proyecto Js'); });
indexRouter.get('/greeting/:nombre', function(req, res) {
    res.status(200).json({ mensaje: '¡Hello ' + req.params.nombre });
})

export default  indexRouter;