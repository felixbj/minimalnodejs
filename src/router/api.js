import express from "express"
import apiControllers from "../controllers/apiControllers"

const apiRouter = express.Router();

apiRouter.route('/:id')
    .get((req, res) => { apiControllers.getOne(req, res) })
    .put((req, res) => { apiControllers.updateOne(req, res) })
    .delete((req, res) => { apiControllers.deleteOne(req, res) });
apiRouter.route('/')
    .get((req, res) => { apiControllers.getAll(req, res); })
    .post ((req, res) => { apiControllers.saveOne(req, res);})
    .put((req, res) => { apiControllers.updateOne(req, res) })
    .delete((req, res) => { apiControllers.deleteOne(req, res) });

export default  apiRouter;