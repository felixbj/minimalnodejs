import express from "express"
import dotenv  from "dotenv"
import Logger from "logger-nodejs"
import indexRouter from "./router"
import apiRouter from "./router/api"
import { DEFAULT_PORT } from "./config/constats"

dotenv.config()

const app = express();
const normalizePort = (val) => {
    const port = parseInt(val, 10);
    if (Number.isNaN(port)) {
        return parseInt(DEFAULT_PORT, 10);
    }
    return port;
};
const port = normalizePort(process.env.PORT || DEFAULT_PORT)
const logger = new Logger();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/api', apiRouter);
app.use('*', function(req, res){ res.status(404).send('not-found'); });// ruta no existe
app.use(function(err, req, res, next) {// manejando error
    console.error(err.stack);
    res.status(500).send('Algo salio mal');
});

app.set("port", port);
app.listen(app.get("port"),() => {
    logger.info(`Servidor escuchando en el puerto: ${ app.get("port")}`);
});

module.exports = app;

//app.use(express.static(__dirname + "./public"))
//app.set('views', path.join(__dirname, 'views'))