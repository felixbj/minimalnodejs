//import model

exports.getAll = function () {
    return { status: 200, mensaje: `API get`};
};
exports.getOne = function (req) {
    return { status: 200, mensaje: `API get con el id: ${ req.params.id }`};
};
exports.saveOne = function (req) {
    return { status: 200, mensaje: `API post con el body: ${ req.body }`};
};
exports.updateOne = function (req) {
    return { status: 200, mensaje: `API put con el id: ${ req.params.id }`};
};
exports.deleteOne = function (req) {
    return { status: 200, mensaje: `API delete con el id: ${ req.params.id }`};
};